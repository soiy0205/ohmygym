<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="//s.wemep.co.kr/ui/v2.7.13/dist/pc/css/common/common.css">
<link rel="stylesheet"
	href="//s.wemep.co.kr/ui/v2.7.13/dist/pc/css/common/layout.css">
<link rel="stylesheet"
	href="//s.wemep.co.kr/ui/v2.7.13/dist/pc/css/pages/mix_common.css">
<link rel="stylesheet"
	href="//s.wemep.co.kr/front/assets/css/motion.css?20200601_1">
<link rel="stylesheet"
	href="//s.wemep.co.kr/ui/v2.7.13/dist/pc/css/pages/member.css">
<script src="//s.wemep.co.kr/front/assets/js/pc/vendor.js?20200601_1"></script>
<script src="//s.wemep.co.kr/front/assets/js/pc/plugins.js?20200601_1"></script>

<style>
.login_wrap {
	margin-top: 100px;
}

.login_wrap .tab_info .on a {
	height: 14px;
	padding: 19px 0 21px 0;
	border-width: 2px 2px 0 2px;
	color: #333;
}

input#CertificationNum:focus {
	outline: none;
}
</style>

</head>
<body>

	<%@ include file="../common/followerNav.jsp"%>
	<section style="height:800px;">
		<div class="login_wrap">
			<h4 class="tit t_find_id_pw">아이디/비밀번호 찾기</h4>
			<div class="login_inner">
				<div class="wrap_tab">
					<ul id="_findTab" class="tab_info">
						<li id="idfind"><a
							class="tab1"> <span>아이디 찾기</span>
						</a></li>
						<li class="on" id="pwdfind"><a class="tab2">
								<span>비밀번호 찾기</span>
						</a></li>
					</ul>
					<div id="_tab2" class="tab_cont">
						<h5 class="blind">비밀번호 찾기</h5>
						<div class="find_gate">
							<i class="ico ico_phone"></i>
							<p class="title">인증번호 입력</p>
							<p class="text_small">핸드폰 문자로 보내드린 인증번호를 입력해주세요.</p>
							<br>
							<form action="" method="post">
								<input type="text" id="CertificationNum" name="CertificationNum" style="height: 35px; width: 130px; border-radius: 10px; border: 1px solid black;" placeholder="인증번호"><br> <a onclick=""
									class="btns_sys red_mid_d" data-find-btn="idGatePhone"><span>확인</span></a>
							</form>
						</div>
					</div> 
				</div>
			</div>
		</div>
	</section>
	<footer>
		<%@ include file="../common/footer1.jsp"%>
	</footer>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<title>Insert title here</title>
 <style>
         html {
            width: 1440px;
            margin: 0;
        }
        * {
            margin: 0;
            padding: 0;
            font-family: "Noto Sans KR Medium";
            text-decoration: none;
        }
        body {
            width: 1440px;
            background-color: lightgray;
            margin-right: 0;
        }
        header {
            width: 1440px;
            height: 63px;
            margin-right: 0;
        }
        #top_background {
            position: absolute;
            left: 0;
            top : 0;
            display: inline;
            z-index: -100;
        }
        #logo {
            position: absolute;
            top: 13px;
            left: 13px;
        }
        header div{
            position: relative;
            /* left: 1000px; */
            font-size: 13px;
            color: white;
            top: 22px;
        }
        header div a {
            font: bold 13px "Noto Sans KR Thin";
            color: white;
        }
        nav {
            position: absolute;
            top: 63px;
            font-size: 15px;
        }
        #nav_background {
            position: absolute;
            z-index: -100;
            top: 0;
            height: 900%;
            width: 200px;
        }
        nav ul {
            background-color: 000532;
            position: relative;
            font-weight: bold;
            color: white;
            /* left: 30px;*/
            top: 3px;
            list-style: none;
            width: 160px;
            padding-left: 25px;
        }
        nav li {
            color: black;
            font-weight: normal;
            margin: 8px;
        }
        nav a {
            color: white;
            font-size: 13px;
        }
        .menuGroup {
            display: inline-block;
            width: 200px;
            position: absolute;  
        }
        details {
            cursor: pointer;
        }
        summary {
            outline: none;
        }

        /*섹션*/

        .customer_content_table{
            border-radius: 5px;
            background: white;
        }

        .customer_content_container{
            padding: 50px;
        }

        .customer_table{
            width: 1000px;
            height: 350px;
            text-align: center;
        }

        table.customer_table  {
            border-top: 3px solid #444444;
            border-bottom: 3px solid #444444;
            border-collapse: collapse;
            text-align: center;
        }
        table.customer_table th{
            font-family: "Noto Sans KR Thin";
            color: white;
            font-weight: bolder;
        }
        table.customer_table td{
            color: black;
        }

        table.customer_table th, td {
            border-bottom: 1px solid #a9a9a9;
            height: 40px;
        }
        table.customer_table th{
            background: #000532;
        }

        table.cash_table{
            border-top: 3px solid #444444;
            border-bottom: 3px solid #444444;
            width: 450px;
        }
        table.cash_table tr td:nth-child(2n){
            color: white;
            background: #000532;
        }
        table.cash_table tr th:nth-child(2n){
            color: white;
            background: #000532;
        }
        table.cash_table td, th{
            border-bottom: 1px solid #a9a9a9;
        }
        
        table.cash_table th{
            font-size: 0.75em;
            text-align: left;
            font-weight:600;
        }

        table.cash_table td{
            font-size: 1.25em;
            text-align: center;
            font-weight: 900;
        }

        table.customer_cash_list  {
            width: 1000px;
            border-top: 3px solid #444444;
            border-bottom: 3px solid #444444;
            border-collapse: collapse;
            text-align: center;
            margin-left: 30px;
            margin-right: 30px;
        }
        table.customer_cash_list th, td {
            border-bottom: 1px solid #444444;
        }
    </style>
</head>
<body>
    <header>
        <img src="../../resources/imageManager/top.png" id="top_background">
        <a href="#" id="logo"><img src="../../resources/imageManager/logo.png" alt="logo"></a>
        <div style="width: 1390px;" align="right">
            <a href="#">개발요청페이지</a> &nbsp;&nbsp;&nbsp;OhMyGym님 환영합니다. 
            &nbsp;&nbsp;&nbsp;<a href="#">로그아웃</a>
        </div>
    </header>
    <nav>
        <div><img src="../../resources/imageManager/menuGroup.png" class="menuGroup" ></div>
        <ul>
        	<details open><summary>회원관리</summary>
            <li><a href="#">고객조회/수정</a></li><br>
           	</details>
        </ul>

        <img src="../../resources/imageManager/menuGroup.png" class="menuGroup">
        <ul><details open><summary>트레이너 관리</summary>
            <li><a href="#">트레이너조회/수정</a></li>
            <li><a href="#">트레이너 승인요청관리</a></li>
            <li><a href="#">비용지급관리</a></li><br>
            </details>
        </ul>
        <img src="../../resources/imageManager/menuGroup.png" class="menuGroup">
        <ul><details open><summary>트레이닝룸 관리</summary>
            <li><a href="#">트레이닝룸 조회</a></li>
            <li><a href="#">트레이닝 진행 이력 관리</a></li><br>
            </details>
        </ul>
        
        <img src="../../resources/imageManager/menuGroup.png" class="menuGroup">
        <ul><details open><summary>그룹소통방 관리</summary>
            <li><a href="#">그룹소통방 조회</a></li><br>
            </details>
        </ul>
        
        <img src="../../resources/imageManager/menuGroup.png" class="menuGroup">
        <ul><details open><summary>식단페이지 관리</summary>
            <li><a href="#">회원별 식단 조회</a></li><br>
            </details>
        </ul>
        
        <img src="../../resources/imageManager/menuGroup.png" class="menuGroup">
        <ul><details open><summary>건강정보게시판 관리</summary>
            <li><a href="#">게시글작성/조회/수정</a></li><br>
            </details>
        </ul>
        
        <img src="../../resources/imageManager/menuGroup.png" class="menuGroup">
        <ul><details open><summary>챌린지 관리</summary>
            <li><a href="#">챌린지 목록 관리</a></li>
            <li><a href="#">챌린지 참여자 관리</a></li><br>
            </details>
        </ul>
        
        <img src="../../resources/imageManager/menuGroup.png" class="menuGroup">
        <ul><details open><summary>고객센터</summary>
            <li><a href="#">공지사항</a></li>
            <li><a href="#">이벤트</a></li>
            <li><a href="#">자주묻는질문</a></li>
            <li><a href="#">직접묻는질문</a></li>
            <li><a href="#">신고내역관리</a></li>
            <li><a href="#">스탬프관리</a></li><br>
            </details>
        </ul>
        
        <img src="../../resources/imageManager/menuGroup.png" class="menuGroup">
        <ul><details open><summary>OH머니 관리</summary>
            <li><a href="#">지급/사용내역</a></li>
            <li><a href="#">수기지급</a></li>
            <li><a href="#">수기지급내역관리</a></li>
            </details>
        </ul>
        <img src="../../resources/imageManager/menu_bar.png" id="nav_background">
    </nav>

    <section style="position: absolute; left: 210px">
        <div class="customer_content_container">
            <div class="customer_content_main">
                <p style="font-weight: bold; font-size: 1.5em; display:block; float: left;">고객상세정보</p>
                <button style="display:block; width: 105px; height: 35px; font-size: 0.7em; background: orangered; font-weight: bold; color: white; float: right; margin-right: 10px; border: none; border-radius: 5px;">임시비밀번호발급</button>
                <button style="display:block; width: 105px; height: 35px; font-size: 0.7em; background: #000532; font-weight: bold; color: white; float: right; margin-right: 10px;  border: none; border-radius: 5px;">Edit</button>
                <button style="display: block; float: right; width: 105px; height: 35px; font-size: 0.7em; background: #000532; font-weight: bold; color: white; float: right; margin-right: 10px;  border: none; border-radius: 5px;"">목록</button>
                <br><br>
                <div class="customer_content_table" style="padding: 15px; clear: both;">
                    <table border="1" class="customer_table" style="margin-left: 30px; margin-right: 30px; margin-bottom: 30px;">
                        <tbody>
                            <tr>
                                <th>프로필</th>
                                <th>아이디</th>
                                <td><input type="text" value="ID" style="outline: none; border: none; background: none;" readonly></td>
                                <th>회원상태</th>
                                <td><input type="text" value="정상"  style="outline: none;  border: none;  background: none;" readonly></td>
                            </tr>
                            <tr>
                                <td colspan="1" rowspan="4">
                                    <img class="customer_profileImg" width="160px" height="180px" src="">
                                </td>
                                <th>닉네임</th>
                                <td><input type="text" value="감자" style="outline: none; border: none; background: none;" readonly></td>
                                <th>등록여부</th>
                                <td><input type="text" value="Y"  style="outline: none;  border: none;  background: none;" readonly></td>
                            </tr>   
                            <tr>
                                <th>회원명</th>
                                <td><input type="text" value="이해림" style="outline: none; border: none; background: none;" readonly></td>
                                <th>가입일</th>
                                <td><input type="text" value="2020/06/15"  style="outline: none;  border: none;  background: none;" readonly></td>
                            </tr>
                            <tr>
                                <th>이메일</th>
                                <td><input type="text" value="hlm1225@kh.or.kr" style="outline: none; border: none; background: none;" readonly></td>
                                <th>연락처</th>
                                <td><input type="text" value="010-1234-4156"  style="outline: none;  border: none;  background: none;" readonly></td>
                            </tr>
                            <tr>
                                <th>주소</th>
                                <td colspan="3"><input type="text" value="경기도 용인시 기흥구 피로동 8234번지 편안한 아파트 128동 802호" style="width: 100%; outline: none; border: none; background: none;" readonly></td>
                            </tr>
                            <tr>
                                <th>건강정보</th>
                                <td colspan="4"></td>
                            </tr>
                            <tr>
                                <th>메모</th>
                                <td colspan="4"></td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="login_wrap" style="display: none;">
                        <div class="dark_bg" onclick="jQuery('.login_wrap').fadeOut('slow')"></div>
                        <div class="login_box">
                            <div style="width: 100%; height: 30px; background: orangered; border-radius: 8px;">
                                <div style="color: white; font-weight: bold; font-family: 'Noto Sans KR'; margin-left: 270px; padding-top: 5px;">오머니 충전</div>
                                <img class="close" onclick="jQuery('.login_wrap').fadeOut('slow')" src="img_icon/closeIcon.png" width="20px" height="20px">
                            </div>
                            <div class="login_content">
                            </div>
                        </div>
                    </div>
                    
                    <div class="customer_ohMoney_content" style="text-align: center; margin: auto;">
                        <table  class="cash_table"  style="margin-left: 325px;"> 
                            <tbody>
                                <tr>
                                    <th>
                                      	현재 보유 오머니
                                    </th>
                                    <th>
                                       	환급 불가 오머니
                                    </th>
                                    <th>
                                       	환급 가능 오머니
                                    </th>
                                </tr>
                                <tr>
                                    <td>50,000원
                                    </td>
                                    <td>40,000원
                                    </td>
                                    <td>10,000원
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div>
                        <p style="font-size: 1.25em; font-weight:600; margin-left: 30px; margin-bottom: 10px;">오머니 내역</p>
                        <table class="customer_cash_list">
                            <thead>
                                <tr>
                                    <th>
                                        NO.
                                    </th>
                                    <th>
                                        	일시
                                    </th>
                                    <th>
                                        	구분
                                    </th>
                                    <th> 
                                        	내역
                                    </th>
                                    <th>
                                        	금액
                                    </th>
                                    <th>
                                        	잔액
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <!--리스트구분넘버-->1
                                    </td>
                                    <td>
                                        <!--오머니 변동일시-->2020.07.03
                                    </td>
                                    <td>
                                        <!--오머니 변동구분-->충전
                                    </td>
                                    <td>
                                        <!--오머니 변동세부내역-->카드결제로 인한 오머니 충전
                                    </td>
                                    <td>
                                        <!--오머니 변동액-->10000
                                    </td>
                                    <td>
                                        <!--남은 오머니-->15000
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
</html>
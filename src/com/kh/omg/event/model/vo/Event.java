package com.kh.omg.event.model.vo;

import java.sql.Date;

public class Event implements java.io.Serializable{
	
	private String eventIng;
	private String eventEnd;
	private Date date;
	
	public Event() {}

	public Event(String eventIng, String eventEnd, Date date) {
		super();
		this.eventIng = eventIng;
		this.eventEnd = eventEnd;
		this.date = date;
	}

	public String getEventIng() {
		return eventIng;
	}

	public void setEventIng(String eventIng) {
		this.eventIng = eventIng;
	}

	public String getEventEnd() {
		return eventEnd;
	}

	public void setEventEnd(String eventEnd) {
		this.eventEnd = eventEnd;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Event [eventIng=" + eventIng + ", eventEnd=" + eventEnd + ", date=" + date + "]";
	}
	
	
	

}
